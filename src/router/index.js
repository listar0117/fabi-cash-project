import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '@/views/HomePage.vue'
import CompanyPage from '@/views/CompanyPage.vue'
import AboutPage from '@/views/AboutPage.vue'
import ManagementPage from '@/views/ManagementPage.vue'
import EventsPage from '@/views/EventsPage.vue'
import NewsPage from '@/views/NewsPage.vue'
import ProposalGuarantee from '@/views/ProposalGuarantee.vue'
import TestemonialsPage from '@/views/TestemonialsPage.vue'
import PrivacyPolicy from '@/views/PrivacyPolicy.vue'
import EndUserAgreement from '@/views/EndUserAgreement.vue'
import SolutionsPage from '@/views/SolutionsPage.vue'
import FabiCashPage from '@/views/FabiCashPage.vue'
import FabiTrackPage from '@/views/FabiTrackPage.vue'
import FabiSportPage from '@/views/FabiSportPage.vue'
import FabiAtmKiosk from '@/views/FabiAtmKiosk.vue'
import FabiCashMobile from '@/views/FabiCashMobile.vue'
import SupportPage from '@/views/SupportPage.vue'
import GivingBack from '@/views/GivingBack.vue'
import ContactPage from '@/views/ContactPage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home-page',
    component: HomePage
  },
  {
    path: '/company',
    name: 'company-page',
    component: CompanyPage,
  },
  {
    path: '/about',
    name: 'about-page',
    component: AboutPage
  },
  {
    path: '/management',
    name: 'management-page',
    component: ManagementPage
  },
  {
    path: '/events',
    name: 'events-page',
    component: EventsPage
  },
  {
    path: '/news',
    name: 'news-page',
    component: NewsPage
  },
  {
    path: '/proposal-guarantee',
    name: 'proposal-guarantee',
    component: ProposalGuarantee
  },
  {
    path: '/testemonials',
    name: 'testemonials',
    component: TestemonialsPage
  },
  {
    path: '/privacy-policy',
    name: 'privacy-policy',
    component: PrivacyPolicy
  },
  {
    path: '/end-user-agreement',
    name: 'end-user-agreement',
    component: EndUserAgreement
  },
  {
    path: '/solutions',
    name: 'solutions',
    component: SolutionsPage
  },
  {
    path: '/fabi-cash',
    name: 'fabi-cash',
    component: FabiCashPage
  },
  {
    path: '/fabi-track',
    name: 'fabi-track',
    component: FabiTrackPage
  },
  {
    path: '/fabi-sport',
    name: 'fabi-sport',
    component: FabiSportPage
  },
  { 
    path: '/atm-and-kiosk',
    name: 'atm-and-kiosk',
    component: FabiAtmKiosk
  },
  {
    path: '/mobile',
    name: 'mobile',
    component: FabiCashMobile
  },
  {
    path: '/support',
    name: 'support',
    component: SupportPage
  },
  {
    path: '/giving-back',
    name: 'giving-back',
    component: GivingBack
  },
  {
    path: '/contact',
    name: 'contact',
    component: ContactPage
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

export default router
