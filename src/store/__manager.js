export default {
  state: {
    data: [
      {
        full_name: 'ANTHONY RABITO, JR.',
        title: 'President and Chief Executive Officer',
        history: 'Anthony founded our company in April, 1996. Self employed since college, Anthony is an entrepreneur in every sense of the word. It is because of his vision, passion, and hard work that FABICash® has become the leading cash access provider in the gaming industry. Anthony is an avid golfer and business leader in his community.',education: 'Anthony has a B.S. degree in Business Administration from Louisiana State University.',
        avata_image: 'manager1.jpg'
      },
      {
        full_name: 'KAYLA HERNANDEZ',
        title: 'Executive Controller',
        history: 'Kayla Hernandez joined the FABICash® team in 2011.',
        education: 'She holds a B.S. degree in Mathematics from the University of New Orleans and a Master of Finance degree from Tulane University.',
        avata_image: 'manager2.jpg'
      },
      {
        full_name: 'ANTHONY RABITO, III',
        title: 'Executive Vice President of Operations',
        history: 'Anthony’s role in our company involves handling the technology aspect of FABICash® and FABITrack®. He works with our in house engineers, creating new features and technology used in our customers’ casinos. Anthony ensures that operations and services provided to our customers exceed their expectations.',
        education: 'Anthony has a B.S. degree in Business Administration from Our Lady of Holy Cross College. Anthony has been with the FABICash® team since 2005.',
        avata_image: 'manager3.jpg'
      },
      {
        full_name: 'NICHOLAS RABITO',
        title: 'Sales Account Executive',
        history: 'Nick’s role in FABICash® is maintaining our casino partnerships through customer and technical support. Nick prides himself on service after the sale. His drive and dedication to the gaming industry has enhanced the culture of FABICash®.',
        education: 'Nick has a B.S. degree in Business Administration from the University of New Orleans. Nick joined the FABICash® team in 2009.',
        avata_image: 'manager4.jpg'
      }
    ],
  },
}

