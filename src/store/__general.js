export default {
  state: {
    title: '',
    sub_title: '',
  },
  mutations: {
    setTitle (state, data) {
      state.title = data
    },
    setSubTitle (state, data) {
      state.sub_title = data
    }
  },
  actions: {
  }
}

