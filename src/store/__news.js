export default {
  state: {
    data: [
      {
        month: 'MAR',
        date: '13, 2019',
        title: 'FABICash Donates $20,000 to the Wyandotte Nation',
        description: 'FABICash is proud to donate $20,000 to the Wyandotte Nation Education Trust Fund on March 20, 2019.',
        img: 'news1.jpg'
      },
      {
        month: 'JAN',
        date: '30, 2019',
        title: 'FABICash Donates $20,000 to The Tomorrow Fund',
        description: 'FABICash is proud to donate $20,000 to The Tomorrow Fund, in association with our customer, Twin River Casino, on January 29, 2019.',
        img: 'news2.jpg'
      },
      {
        month: 'JAN',
        date: '23, 2019',
        title: 'FABICash Donates $5,000 to the Chitimacha Tribe Education Trust Fund',
        description: 'FOR IMMEDIATE RELEASE: FABICash donates $5,000 for Native American Indian Education Charenton, Louisiana- January 22, 2019- FABICash is proud to donate $5,000 to the Chitimacha Tribe of Louisiana’s Educational Trust Fund. Anthony Rabito, III, Vice President of Operations at FABICash, presented the check to the Chitimacha Tribe Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo laudantium distinctio cupiditate beatae sed quis assumenda doloremque unde vitae pariatur asperiores possimus voluptate aperiam quo iure, deserunt obcaecati. Placeat, a.',
        img: 'news3.jpg'
      },
      {
        month: 'SEPT',
        date: '20, 2018',
        title: 'FABICash Partners with Secure Trading to Develop FABISport – Online Sports Betting Solution',
        description: 'U.S. Sports Betting Has a New E2E Platform Solution Launching in Casinos with Online Player Deposits and Winnings to Support a $6Bn Market by 2023. FABICash Partners with Secure Trading to Create FABISport® Secure Trading, a leader in online gaming with its comprehensive compliance and payments platform, Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed commodi neque vel ipsa perspiciatis expedita. Ipsa, iure corrupti? Tempore natus blanditiis laboriosam nisi, accusamus assumenda vitae quisquam magni necessitatibus reiciendis?',
        img: 'news4.jpg'
      },
      {
        month: 'JULY',
        date: '26, 2018',
        title: 'FABICash Donates $5,000 to the Otoe-Missouria Tribe Education Fund',
        description: 'FABICash is proud to donate $5,000 to the Education Trust Fund of our casino customer, Seven Clans, on July 24, 2018 at the OIGA trade show.',
        img: 'news5.jpg'
      },
    ]
  }
}