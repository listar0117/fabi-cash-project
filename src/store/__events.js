export default {
  state: {
    data: [
      {
        month: 'JAN',
        date: '16 - 18, 2019',
        title: 'San Diego AML Seminar',
        address: 'San Diego, CA',
        sub_address: ''
      },
      {
        month: 'FEB',
        date: '4 – 7, 2019',
        title: 'WIGC - Western Indian Gaming Conference',
        address: 'Valley Center, CA',
        sub_address: 'Booth #309'
      },
      {
        month: 'FEB',
        date: '26 – 27, 2019',
        title: 'Oklahoma AML Seminar',
        address: 'Catoosa, OK',
        sub_address: ''
      },
      {
        month: 'APRIL',
        date: '1 – 4, 2019',
        title: 'NIGA – Indian Gaming Tradeshow',
        address: 'San Diego, CA',
        sub_address: 'Booth #1241'
      },
      {
        month: 'MAY',
        date: '7 – 9, 2019',
        title: 'SGS – Southern Gaming Summit',
        address: 'Biloxi, MS',
        sub_address: 'Booth #2'
      },
      {
        month: 'MAY',
        date: '7 – 9, 2019',
        title: 'Niagara Falls AML Seminar',
        address: 'Niagara Falls, NY',
        sub_address: ''
      },
      {
        month: 'MAY',
        date: '13 – 15, 2019',
        title: 'ICE North America',
        address: 'Boston , MA',
        sub_address: 'BOOTH #D15'
      },
      {
        month: 'JUNE',
        date: '6 – 7, 2019',
        title: 'Washington AML Seminar',
        address: 'Tacoma , WA',
        sub_address: ''
      },
      {
        month: 'JULY',
        date: '17 – 18, 2019',
        title: 'Cage Operations Conference & Expo',
        address: 'Las Vegas , NV',
        sub_address: ''
      },
      {
        month: 'JULY',
        date: '22 – 24, 2019',
        title: 'OIGA - Oklahoma Indian Gaming Tradeshow',
        address: 'Tulsa , OK',
        sub_address: 'BOOTH #515/517'
      },
      {
        month: 'AUG',
        date: '12 – 15, 2019',
        title: 'National AML Seminar',
        address: 'Las Vegas , NV',
        sub_address: ''
      },
      {
        month: 'OCT',
        date: '15 – 17, 2019',
        title: 'G2E - Global Gaming Expo',
        address: 'Las Vegas , NV',
        sub_address: 'BOOTH #1632'
      },
      {
        month: 'NOV',
        date: '2 – 4, 2019',
        title: 'California Gaming Association TradeShow',
        address: 'Ranchos Palos Verdes, CA',
        sub_address: ''
      },
      {
        month: 'NOV',
        date: '11 – 14, 2019',
        title: 'TribalNet Conference & Tradeshow',
        address: 'Nashville, TN',
        sub_address: ''
      },
    ]
  }
}