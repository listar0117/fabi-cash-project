import Vue from 'vue'
import Vuex from 'vuex'

import general from './__general'
import manager from './__manager'
import events from './__events'
import news from './__news'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    general,
    manager,
    events,
    news,
  }
})
