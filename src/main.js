import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
// import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import jQuery from 'jquery'
import Vuelidate from 'vuelidate'
import VueSlideUpDown from 'vue-slide-up-down'

global.$ = global.jQuery = jQuery
Vue.use(Vuelidate)
Vue.component('vue-slide-up-down', VueSlideUpDown)
// require('jquery-slimscroll')
import './assets/scss/styles.scss'
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
