(function() {
	this.threeDots = function(els, handleLinks) {
		window.onresize = function() {
			threeDots(els, handleLinks);
		};

		var nodes = document.getElementsByClassName(els);
		var numOfNodes = nodes.length;
		var nodesHelpers = document.getElementsByClassName('threeDotsHelpers');
		var regex = new RegExp(els);
		var className = String(regex).slice(1, -1);
		var helpersContainer = document.getElementById('threeDotsHelpersContainer');

		if (!helpersContainer) {
			helpersContainer = createHelpersContainer();
		}

		for (var i = 0; i < numOfNodes; i++) {
			var cl = nodes[i].getAttribute('class').split(' ');
			var classIndex = -1;
			var num = -1;

			for (var j = 0; j < cl.length; j++) {
				var elNum = Number(cl[j].replace(regex, ''));

				if (!isNaN(elNum) && elNum !== 0) {
					classIndex = j;
					num = elNum;
				}
			}

			// if threeDotsHelpers container for this node has been created already
			if (classIndex !== -1) {
				// just copy text from needed helper container into the node
				nodes[i].innerHTML = document
					.getElementById('threeDotsHelpersContainer')
					.querySelector('.' + className + String(num))
					.querySelector('.text-container').innerHTML;
			} else {
				// create a helper container for the node and copy the node's text and "Read more" link (if it exists) into the helper container
				var newClass = className + String(nodesHelpers.length + 1);
				nodesHelpers = document.getElementsByClassName('threeDotsHelpers');
				var newNode = document.createElement('p');

				if (handleLinks) {
					var link = nodes[i].querySelector('a');

					if (link) {
						link = link.cloneNode(true);
						popLink(nodes[i]);
					}
				}

				var innerTextNode = document.createElement('span');
				innerTextNode.setAttribute('class', 'text-container');
				var textNode = document.createTextNode(nodes[i].innerHTML);
				innerTextNode.appendChild(textNode);
				newNode.appendChild(innerTextNode);

				if (handleLinks) {
					if (link) {
						newNode.appendChild(link);
					}
				}

				newNode.setAttribute('class', 'threeDotsHelpers ' + newClass);

				helpersContainer.appendChild(newNode);

				nodes[i].setAttribute('class', className + ' ' + newClass);
			}
		}

		for (var i = 0; i < numOfNodes; i++) {
			if (isOverflowed(nodes[i])) {
				if (handleLinks) {
					popLink(nodes[i]);
				}

				var sHeight = nodes[i].scrollHeight;
				var cHeight = nodes[i].clientHeight;
				var pointOfTruncating = nodes[i].innerHTML.slice(0, parseInt(nodes[i].innerHTML.length * (cHeight / sHeight)));

				nodes[i].innerHTML = pointOfTruncating + '[&hellip;] ';

				if (handleLinks) {
					appendLink(nodes[i], className, i);
				}

				// console.log(nodes[i]);

				while (isOverflowed(nodes[i])) {
					if (handleLinks) {
						popLink(nodes[i]);
					}

					nodes[i].innerHTML = nodes[i].textContent.slice(0, -5) + '[&hellip;] ';

					if (handleLinks) {
						appendLink(nodes[i], className, i);
					}
				}
			}
		}
	};

	function isOverflowed(el) {
		return el.scrollHeight > el.clientHeight || el.scrollWidth > el.clientWidth;
	}

	function createHelpersContainer() {
		var newNode = document.createElement('div');
		newNode.setAttribute('id', 'threeDotsHelpersContainer');
		newNode.style.display = 'none';
		document.body.appendChild(newNode);

		return document.getElementById('threeDotsHelpersContainer');
	}

	function appendLink(node, className, i) {
		var link = document
			.getElementById('threeDotsHelpersContainer')
			.querySelector('.' + className + String(i + 1))
			.querySelector('a');

		if (link) {
			link = link.cloneNode(true);
			node.appendChild(link);
		}
	}

	function popLink(node) {
		while (node.lastChild && node.lastChild.nodeType !== Node.TEXT_NODE) {
			node.removeChild(node.lastChild);
		}
	}
})();

var c = null;
var container = null;
var ctx = null;
var percent = -10;
var direction = 0.05;
var fps = 60;
var percent1 = 110;
var direction1 = -0.05;

if ($('.hero-section').length) {
	window.c = document.querySelector('#canvas');
	window.container = document.querySelector('.hero-section');
	window.ctx = c.getContext('2d');
}

function adjuster() {
	activateSubmenus();

	if ($('.hero-section').length) {
		initializeHeroSection();
	}

	initializeBodyAdjustment();
	initializeMobileMenu();
	adjustInnerPageMainContent();
	initializeThreeDots();
	initializeScrollToTop();
	adjustCompanyImages();
	showCurrentMenuItem();
	initializeContactForm();

	if ($('.hero-section').length) {
		animate();
	}
}

function closeSubmenu(menu, timing) {
	// var submenu = $(menu).find('.submenu');

	// $(menu).removeClass('active');
	// $(submenu).slideUp(timing);
}

function openSubmenu(menu, timing) {
	// var submenu = $(menu).find('.submenu');

	// $(menu).addClass('active');
	// $(submenu).slideDown(timing);
}

function activateSubmenus() {
	closeSubmenu($('.has-submenu'), 0);

	//console.log('[] activateSubmenus');

	$('.parent-nav').on('click', function(event) {
		// event.preventDefault();
		// console.log($(this)[0]);
		// console.log(this);
		// closeSubmenu($('.has-submenu'), 0);
		toggleSubmenu($(this));
	});

	$('.has-submenu > a').on('click', function(event) {
		// event.preventDefault();
		// console.log($(this)[0]);
		// console.log(this);
		toggleSubmenu($(this).closest('.has-submenu'));
	});
}

$('.parent-nav').click(function(e) {
	// window.location.href = $(this).parent()[0].href;
	// e.preventDefault();
});

function toggleSubmenu(menu) {
	// console.log('[] togglesubmenu');
	if ($(menu).hasClass('active')) {
		// console.log('closing sub menu');
		closeSubmenu(menu, 500);
	} else {
		// console.log('opening sub menu');
		openSubmenu(menu, 500);
	}
}

function initializeHeroSection() {
	$(window).on('resize', adjustHeroSection);

	adjustHeroSection();
}

function adjustHeroSection() {
	var section = $('.hero-section');
	var width = Number($(section).width());
	var height = String(width * 0.7);

	// if ($(window).width() < 550) {
	//     height = String(width * 1.6) + "px";
	// }

	$(section).css('height', height);
}

function initializeBodyAdjustment() {
	adjustBodyHeight();
	adjustNavbarFooter();

	if ($('.hero-section').length) {
		drawALine();
	}

	$(window).on('resize', function() {
		adjustBodyHeight();
		adjustNavbarFooter();
		adjustInnerPageMainContent();
		adjustCompanyImages();

		if ($('.hero-section').length) {
			drawALine();
		}
	});
}

function adjustBodyHeight() {
	var bodyHeight = $('.navbar').outerHeight();
	var containerHeight = $('.main-content').outerHeight();
	var height = $(window).width() > 991 ? String(containerHeight) + 'px' : String(containerHeight + 65) + 'px';

	// $('.navbar').css('min-height', String(containerHeight) + 'px');
}

function adjustNavbarFooter() {
	var height = $('footer').outerHeight();

	//$('.navbar-footer').css('height', String(height) + 'px');
}

function drawLine(line, ax, ay, bx, by, revert) {
	if (ay > by) {
		bx = ax + bx;
		ax = bx - ax;
		bx = bx - ax;
		by = ay + by;
		ay = by - ay;
		by = by - ay;
	}
	var calc = Math.atan((ay - by) / (bx - ax));
	calc = (calc * 180) / Math.PI;

	slope = Math.abs(calc);

	if (revert) {
		slope = 180 - slope;
	}

	var length = Math.sqrt((ax - bx) * (ax - bx) + (ay - by) * (ay - by));
	$(line).css({
		width: length + 'px',
		top: String(ay) + 'px',
		left: String(ax) + 'px',
		transform: 'rotate(' + slope + 'deg)'
	});
}

function getCityCoordinates(city) {
	var width = $('.hero-section').outerWidth();
	var height = $('.hero-section').outerHeight();
	var x = 0;
	var y = 0;

	if (city == 1) {
		x = width - width * 0.75;
		y = height - height * 0.32;
	} else if (city == 2) {
		x = width * 0.578;
		y = height * 0.647;
	} else if (city == 3) {
		x = width * 0.796;
		y = height * 0.68;
	}

	return {
		x: x,
		y: y
	};
}

function getBoxCoordinate(box, vertical, horizontal) {
	var containerTop = Number($('.logos-container').position().top);
	var containerLeft = Number($('.logos-container').position().left);
	var x = Number($(box).position().left) + containerLeft;
	var y = Number($(box).position().top) + containerTop;

	if (vertical == 'bottom') {
		y += Number($(box).outerHeight());
	}

	if (horizontal == 'right') {
		x += Number($(box).outerWidth());
	}

	return {
		x: x,
		y: y
	};
}

function drawALine() {
	var line = $('.line')[0];
	var box = $('#logo1');
	var boxCoordinates = getBoxCoordinate(box, 'top', 'left');
	var cityCoordinates = getCityCoordinates(1);

	drawLine(line, boxCoordinates.x, boxCoordinates.y, cityCoordinates.x, cityCoordinates.y, false);

	line = $('.line')[1];
	box = $('#logo1');
	boxCoordinates = getBoxCoordinate(box, 'bottom', 'left');
	cityCoordinates = getCityCoordinates(1);

	drawLine(line, boxCoordinates.x, boxCoordinates.y, cityCoordinates.x, cityCoordinates.y, false);

	line = $('.line')[2];
	box = $('#logo2');
	boxCoordinates = getBoxCoordinate(box, 'top', 'left');
	cityCoordinates = getCityCoordinates(2);

	drawLine(line, boxCoordinates.x, boxCoordinates.y, cityCoordinates.x, cityCoordinates.y, false);

	line = $('.line')[3];
	box = $('#logo2');
	boxCoordinates = getBoxCoordinate(box, 'bottom', 'left');
	cityCoordinates = getCityCoordinates(2);

	drawLine(line, boxCoordinates.x, boxCoordinates.y, cityCoordinates.x, cityCoordinates.y, false);

	line = $('.line')[4];
	box = $('#logo3');
	boxCoordinates = getBoxCoordinate(box, 'top', 'right');
	cityCoordinates = getCityCoordinates(3);

	drawLine(line, boxCoordinates.x, boxCoordinates.y, cityCoordinates.x, cityCoordinates.y, true);

	line = $('.line')[5];
	box = $('#logo3');
	boxCoordinates = getBoxCoordinate(box, 'bottom', 'right');
	cityCoordinates = getCityCoordinates(3);

	drawLine(line, boxCoordinates.x, boxCoordinates.y, cityCoordinates.x, cityCoordinates.y, true);
}

function createLine(line, x1, y1, x2, y2) {
	var distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

	xMid = (x1 + x2) / 2 - distance / 2;
	yMid = (y1 + y2) / 2 - distance / 2;

	slopeInRadians = Math.atan2(y1 - y2, x1 - x2);
	slopeInDegrees = (slopeInRadians * 180) / Math.PI;

	$(line).css({
		width: String(distance) + 'px',
		top: String(yMid) + 'px',
		left: String(xMid) + 'px',
		transform: 'rotate(' + String(slopeInDegrees) + 'deg)'
	});
}

function initializeMobileMenu() {
	$('.nav-trigger, .sheet').on('click', function() {
		toggleMobileMenu();
	});
}

function toggleMobileMenu() {
	if ($('.navbar').hasClass('active')) {
		$('.navbar, .sheet, .nav-trigger').removeClass('active');
		$('body').removeClass('mobile-menu-active');
	} else {
		$('.navbar, .sheet, .nav-trigger').addClass('active');
		$('body').addClass('mobile-menu-active');
	}
}

function animate() {
	percent += direction;
	percent1 += direction1;
	if (percent1 < -10) {
		percent1 = 110;
		// direction = -direction;
	}
	if (percent > 110) {
		percent = -10;
		// direction = -direction;
	}

	draw();

	// request another frame
	setTimeout(function() {
		requestAnimationFrame(animate);
	}, 1000 / fps);
}

function draw() {
	c.width = container.offsetWidth;
	c.height = container.offsetHeight;

	// bottom stelite
	var w = c.width;
	var h = c.height;
	var x1 = 0;
	var y1 = h * 0.42;
	var x2 = w;
	var y2 = h * 0.25;
	var xc = w / 2;
	var yc = y2 + (y2 - y1) / 3;

	ctx.clearRect(0, 0, w, h);

	$('.satelite1').css({
		top: String(y1) + 'px',
		left: String(x1) + 'px'
	});

	ctx.beginPath();
	ctx.strokeStyle = '#5E5E5E';
	ctx.moveTo(x1, y1);
	ctx.quadraticCurveTo(xc, yc, x2, y2);
	ctx.stroke();

	var xy;

	xy = getQuadraticBezierXYatPercent(
		{
			x: x1,
			y: y1
		},
		{
			x: xc,
			y: yc
		},
		{
			x: x2,
			y: y2
		},
		percent / 100
	);

	$('.satelite').css({
		top: String(xy.y - 21) + 'px',
		left: String(xy.x) + 'px'
	});

	// top satelite
	var x1 = 0;
	var y1 = h * 0.2;
	var x2 = w;
	var y2 = h * 0.055;
	var xc = w / 2;
	var yc = y2 + (y2 - y1) / 3;

	$('.satelite2').css({
		top: String(y1) + 'px',
		left: String(x1) + 'px'
	});

	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.quadraticCurveTo(xc, yc, x2, y2);
	ctx.stroke();

	var xy;

	var xy1 = getQuadraticBezierXYatPercent(
		{
			x: x1,
			y: y1
		},
		{
			x: xc,
			y: yc
		},
		{
			x: x2,
			y: y2
		},
		percent1 / 100
	);

	$('.satelite2').css({
		top: String(xy1.y - 21) + 'px',
		left: String(xy1.x) + 'px'
	});
}

function getQuadraticBezierXYatPercent(startPt, controlPt, endPt, percent) {
	var x = Math.pow(1 - percent, 2) * startPt.x + 2 * (1 - percent) * percent * controlPt.x + Math.pow(percent, 2) * endPt.x;
	var y = Math.pow(1 - percent, 2) * startPt.y + 2 * (1 - percent) * percent * controlPt.y + Math.pow(percent, 2) * endPt.y;
	return {
		x: x,
		y: y
	};
}

function adjustInnerPageMainContent() {
	if (!$('body').hasClass('inner-page')) {
		return;
	}

	var $mainContent = $('.main-content .content-top');
	var windowHeight = $(window).height();
	var footerHeight = $('footer').outerHeight();
	var navHeight = $('.mobile-nav').outerHeight();
	var height = String(windowHeight - footerHeight - navHeight) + 'px';

	// console.log(height);

	$($mainContent).css({
		'min-height': height
	});
}

function initializeThreeDots() {
	if (!$('body').hasClass('news')) {
		return;
	}

	threeDots('three-dots-container');
}

function scrollToPosition(position) {
	$('html, body').animate(
		{
			scrollTop: position
		},
		800
	);
}

function initializeScrollToTop() {
	$('.scroll-to-top').on('click', function() {
		scrollToPosition(0);
	});
}

function adjustCompanyImages() {
	if (!$('body').hasClass('company')) {
		return;
	}

	var $container = $('.images-container');
	var maxHeight = $($container)
		.find('.col-33')
		.eq(0)
		.find('img')
		.outerHeight();

	$($container)
		.find('.flex-vertical')
		.css({
			'max-height': String(maxHeight) + 'px'
		});
}

function showCurrentMenuItem() {
	var currentPage = getCurrentPage();
	var menuItems = $('.navigation-container a');

	$(menuItems).each(function() {
		var linkHref = $(this).attr('href');

		if (linkHref == currentPage) {
			activateLink($(this));
			return;
		}
	});
}

function activateLink(link) {
	var subMenu = $(link).closest('.submenu');

	if (!subMenu.length) {
		subMenu = $(link)
			.parent()
			.children('.submenu');

		if (subMenu.length) {
			triggerSubMenuClick(subMenu);
		}
	} else {
		triggerSubMenuClick(subMenu);
	}

	$(link).addClass('current');
}

function triggerSubMenuClick(subMenu) {
	// var innerLink = $(subMenu)
	// 	.closest('.has-submenu')
	// 	.children('a')
	// 	.eq(0);

	// $(innerLink).trigger('click');
}

function getCurrentPage() {
	var href = window.location.href;
	var index = href.lastIndexOf('/');
	var page = href.slice(index + 1, href.length);

	return page;
}

function showLoader() {
	$('.loader')
		.removeClass('active')
		.addClass('inactive');
}

function hideLoader() {
	$('.loader').removeClass('inactive');
}

function initializeContactForm() {
	// if (!$(".contact-form").length) {
	//     return;
	// }
	// $(".modal-custom, .close-modal-custom").on("click", function () {
	//     hideModal($(this).closest(".modal-custom"));
	// });
	// $(".modal-custom .modal-inner").on("click", function (event) {
	//     event.stopPropagation();
	// });
}

function showModal(modal) {
	$(modal).addClass('active');
	$('body').addClass('mobile-menu-active');
}

function hideModal(modal) {
	$(modal)
		.removeClass('active')
		.addClass('inactive');

	setTimeout(function() {
		$(modal).removeClass('inactive');
		$('body').removeClass('mobile-menu-active');
	}, 350);
}

$(document).ready(adjuster);
